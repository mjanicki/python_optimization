#!/usr/bin/env python

import pyximport; pyximport.install()
from cython_primes import c_primes, py_primes
from numba import jit
import cProfile
import pstats


def primes(kmax):
    p = [0] * 10000
    result = []
    if kmax > 10000:
        kmax = 10000
    k = 0
    n = 2
    while k < kmax:
        i = 0
        while i < k and n % p[i] != 0:
            i = i + 1
        if i == k:
            p[k] = n
            k = k + 1
            result.append(n)
        n = n + 1
    return result


@jit
def numba_primes(kmax):
    p = [0] * 10000
    result = []
    if kmax > 10000:
        kmax = 10000
    k = 0
    n = 2
    while k < kmax:
        i = 0
        while i < k and n % p[i] != 0:
            i = i + 1
        if i == k:
            p[k] = n
            k = k + 1
            result.append(n)
        n = n + 1
    return result


def test():
    i = 10000
    print primes(i)[-1]
    print py_primes(i)[-1]
    print c_primes(i)[-1]
    print numba_primes(i)[-1]


if __name__ == '__main__':
    cProfile.run('test()', 'test_stats')
    p = pstats.Stats('test_stats')
    p.strip_dirs().sort_stats('time').print_stats(4)
